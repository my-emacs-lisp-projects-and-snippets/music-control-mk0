;;; package --- summary
;; [>>>]
;; [<<<]
;; [>]
;; [||]
;;; Commentary:
;; NONE
;;; code:
(defun nextSong (button)
  "Callback to when button(BUTTON) gets pressed."
  (emms-player-mpd-next)
  )
(defun prevSong (button)
  "Callback to when button(BUTTON) gets pressed."
  (emms-player-mpd-next)
  )
(defun music-play (button)
  "Callback to when button(BUTTON) gets pressed."
  (emms-player-mpd-next)
  )
(defun music-pause (button)
  "Callback to when button(BUTTON) gets pressed."
  (emms-player-mpd-next)
  )
(define-button-type 'm-button-next-song
  'action 'nextSong
  'follow-link t
  'help-echo "Click Button"
  'help-args "test")
(define-button-type 'm-button-prev-song
  'action 'prevSong
  'follow-link t
  'help-echo "Click Button"
  'help-args "test")
(define-button-type 'm-button-play
  'action 'music-play
  'follow-link t
  'help-echo "Click Button"
  'help-args "test")
(define-button-type 'm-button-pause
  'action 'music-pause
  'follow-link t
  'help-echo "Click Button"
  'help-args "test")
 (make-button 28 33 :type 'm-button-next-song)
 (make-button 37 42 :type 'm-button-prev-song)
 (make-button 46 49 :type 'm-button-play     )
 (make-button 53 57 :type 'm-button-pause    )
(provide 'test)
;;; test.el ends here
